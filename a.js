function helloController($scope){
	$scope.gretting = {text:'hello'};
	$scope.estimation = {blocks:[
		{
			name:"tibco be",
			estimation:"components",
			components:[{
				name:"maquinas de estado",
				description:"maquinas de estado compuestas o no",
				elements:[
					{
						nominal:'facil',
						ponderation:0.9,
						quantity:3,
					},{
						nominal:'normal',
						ponderation:1,
						quantity:4,
					},{
						nominal:'dificil',
						ponderation:2,
						quantity:1,
					}
				],
				tasks:[
					{name:"desarrollo",cost:12,profile:"be"},
					{name:"pruebas",cost:15,profile:"be"},
					{name:"análisis",cost:7,profile:"beAnalist"}
				]
			},
			{
				name:"canales",
				description:"canales de comunicación",
				elements:[
					{
						nominal:'entrada',
						ponderation:1.2,
						quantity:1,
					},{
						nominal:'salida',
						ponderation:1,
						quantity:4,
					}
				],
				tasks:[
					{name:"desarrollo",cost:6,profile:"be"},
					{name:"pruebas",cost:10,profile:"be"},
					{name:"análisis",cost:8,profile:"beAnalist"}
				]
			}
			]
		},//block tibco
		{
			name:"formacion",
			estimation:"tasks",
			tasks:[
				{name:"preparar formacion",cost:5,profile:"be"},
				{name:"seminario formativo",cost:2,profile:"be"}
			]
		}
	]};

	$scope.componentElementTotalEffort = function ($component, $element){
		result = 0;
		tasks = $component.tasks;
		for (task in tasks){
			result +=  tasks[task].cost * $element.ponderation *$element.quantity;
		}
		return result;
	};

	$scope.removeComponentElement = function ($elements, $index){
		$elements.splice($index,1);
	};

	$scope.addEmptyComponentElement = function ($elements){
		$elements.push({nominal:"unknown",ponderation:1,quantity:0});
	};

	$scope.componentTotalEffort = function ($component){
		var totalEffot=0;
		for(element in $component.elements){
			totalEffot += $scope.componentElementTotalEffort( $component , $component.elements[element] );
		}
		return totalEffot;
	}

	$scope.blockTotalEffort = function ($block){
		var totalEffot=0;
		if($block.estimation=="components"){
			for(component in $block.components){
				totalEffot +=  $scope.componentTotalEffort($block.components[component]);
			}
		}
		return totalEffot;
	}

	$scope.taskPartial = function ($component, $task){
		var total=0;
		var cost=$component.tasks[$task].cost;
		for (element in $component.elements){
			total += $component.elements[element].quantity * $component.elements[element].ponderation * cost;
		}
		return total;
	}
};
